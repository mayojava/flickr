# Sample Flickr Application

This is a sample application that that queries the Flickr public feeds API

# Architecture
It uses the clean architecture to communicate between layers of the application

* Presentation: This is the view layer that contains the UI and the presenters used to communicate with the domain layer.

* Domain: This contains the business logic. It also exposes interactors called by the presenters in the presentation layer.

* Data: This is the layer that communicates with the data sources. There are two datasources, Local and Remote. The local data source is always queried to return cached results
and then updated with result from remote.

RxJava is used to switch between threads and communication among the layers.

# Libraries used
* Retrofit
* Dagger Android
* Glide
* Mockito
* Room
* RxJava 2