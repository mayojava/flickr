package com.mobile.app.flickr.data

import com.mobile.app.flickr.data.entities.ApiResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET

interface FlickrService {
    @GET("/services/feeds/photos_public.gne?format=json&nojsoncallback=1")
    fun fetchPublicFeed(): Single<ApiResponse>
}