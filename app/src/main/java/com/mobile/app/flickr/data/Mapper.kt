package com.mobile.app.flickr.data

import com.mobile.app.flickr.data.db.FeedEntity
import com.mobile.app.flickr.domain.entities.Feed
import javax.inject.Inject

class Mapper @Inject constructor() {

    fun dbEntityToDomain(feedEntity: FeedEntity): Feed {
        return Feed(
                feedEntity.title,
                feedEntity.link,
                feedEntity.mediaUrl,
                feedEntity.description,
                feedEntity.author,
                feedEntity.published
        )
    }

    fun domainEntityToDbEntity(feed: Feed): FeedEntity {
        return FeedEntity(
                0,
                feed.title,
                feed.link,
                feed.media,
                feed.description,
                feed.author,
                feed.published
        )
    }
 }