package com.mobile.app.flickr.data.cache

import com.mobile.app.flickr.data.Mapper
import com.mobile.app.flickr.data.db.FeedDatabase
import com.mobile.app.flickr.domain.entities.Feed
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class CacheImpl @Inject constructor(
        private val feedDatabase: FeedDatabase,
        private val mapper: Mapper): ICache {

    override fun isDbEmpty(): Single<Boolean> {
        return Single.fromCallable { feedDatabase.feedsDao().recordsCount() }
                .map { count -> count == 0 }
    }

    override fun insertFeeds(feeds: List<Feed>) {
        feeds.forEach { kotlin.run {
            val entity = mapper.domainEntityToDbEntity(it)
            feedDatabase.feedsDao().insertFeed(entity)
        } }
    }

    override fun getFeeds(): Single<List<Feed>> {
        return Single.fromCallable { kotlin.run {
            val feeds = feedDatabase.feedsDao().getAllFeeds()
            val list = mutableListOf<Feed>()
            feeds.forEach { list.add(mapper.dbEntityToDomain(it)) }
            list
        } }
    }

    override fun deleteFeeds() {
        return feedDatabase.feedsDao().deleteAllFeeds()
    }
}
