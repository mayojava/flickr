package com.mobile.app.flickr.data.cache

import com.mobile.app.flickr.domain.entities.Feed
import io.reactivex.Completable
import io.reactivex.Single

interface ICache {
    fun getFeeds(): Single<List<Feed>>

    fun deleteFeeds()

    fun insertFeeds(feeds: List<Feed>)

    fun isDbEmpty(): Single<Boolean>
}