package com.mobile.app.flickr.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

@Database(entities = arrayOf(FeedEntity::class), version = 1)
abstract class FeedDatabase: RoomDatabase() {
    abstract fun feedsDao(): FeedsDao
}