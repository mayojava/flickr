package com.mobile.app.flickr.data.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "feeds")
data class FeedEntity(
        @PrimaryKey(autoGenerate = true)
        val id: Long,
        val title: String,
        val link: String,
        val mediaUrl: String,
        val description: String,
        val author: String,
        val published: String
)