package com.mobile.app.flickr.data.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import io.reactivex.Flowable

@Dao
interface FeedsDao {
    @Insert
    fun saveFeeds(feedEntities: List<FeedEntity>)

    @Insert
    fun insertFeed(feedEntity: FeedEntity)

    @Query("SELECT * FROM feeds")
    fun getAllFeeds(): List<FeedEntity>

    @Query("DELETE FROM feeds")
    fun deleteAllFeeds()

    @Query("SELECT COUNT(*) FROM feeds")
    fun recordsCount(): Int
}