package com.mobile.app.flickr.data.entities

import com.google.gson.annotations.SerializedName

data class ApiResponse(
        val title: String,
        val link: String,
        val description: String,
        val modified: String,
        val generator: String,
        val items: List<FeedItem>
)

data class FeedItem(
    val title: String,
    val link: String,
    val media: Media,
    val date_taken: String,
    val description: String,
    val published: String,
    val author: String,
    val author_id: String,
    val tags: String = ""
)

data class Media(
        @SerializedName("m") val imageUrl: String
)