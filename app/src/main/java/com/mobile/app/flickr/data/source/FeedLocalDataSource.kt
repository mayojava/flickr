package com.mobile.app.flickr.data.source

import com.mobile.app.flickr.data.cache.ICache
import com.mobile.app.flickr.domain.entities.Feed
import com.mobile.app.flickr.domain.response.Response
import io.reactivex.Single
import javax.inject.Inject

class FeedLocalDataSource @Inject constructor(private val cache: ICache): IFeedDataSource {
    override fun getFeeds(): Single<Response<List<Feed>>> {
        return cache.getFeeds()
                .map { feeds -> Response.Success(feeds) }
    }
}