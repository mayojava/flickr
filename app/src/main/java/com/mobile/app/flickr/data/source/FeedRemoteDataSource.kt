package com.mobile.app.flickr.data.source

import com.mobile.app.flickr.data.FlickrService
import com.mobile.app.flickr.data.cache.ICache
import com.mobile.app.flickr.domain.entities.Feed
import com.mobile.app.flickr.domain.exceptions.NoNetworkException
import com.mobile.app.flickr.domain.response.Response
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class FeedRemoteDataSource @Inject constructor(
        private val flickrService: FlickrService,
        private val cache: ICache): IFeedDataSource {

    override fun getFeeds(): Single<Response<List<Feed>>> {
        return flickrService.fetchPublicFeed()
                .map { response -> response.items }
                .flatMapObservable { Observable.fromIterable(it) }
                .map {
                        (title, link, media, _, description, published, author) ->
                            Feed(title, link, media.imageUrl, description, author, published)
                }
                .toList()
                .map<Response<List<Feed>>> { list -> Response.Success(list) }
                .onErrorReturn { throwable -> kotlin.run {
                    if (throwable is NoNetworkException) {
                        Response.NoInternetError()
                    } else {
                        Response.Error(throwable)
                    }
                } }
                .doAfterSuccess({ it -> kotlin.run {
                    if (it is Response.Success) {
                        cache.deleteFeeds()
                        cache.insertFeeds(it.data)
                    }
                }})
    }
}