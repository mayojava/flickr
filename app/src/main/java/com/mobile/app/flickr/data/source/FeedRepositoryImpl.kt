package com.mobile.app.flickr.data.source

import com.mobile.app.flickr.domain.entities.Feed
import com.mobile.app.flickr.domain.repository.IFeedRepository
import com.mobile.app.flickr.domain.response.Response
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Named

class FeedRepositoryImpl @Inject constructor(
        @Named("REMOTE") private val remoteDataSource: IFeedDataSource,
        @Named("LOCAL") private val localDataSource: IFeedDataSource): IFeedRepository  {

    override fun getPublicFeeds(): Observable<Response<List<Feed>>> {
        val pub = remoteDataSource.getFeeds()
                .toObservable()
                .publish(
                        {
                            network ->
                                Observable.merge(
                                        network,
                                        localDataSource.getFeeds().toObservable().takeUntil(network)
                                )
                        }
                )
        return pub
    }

}