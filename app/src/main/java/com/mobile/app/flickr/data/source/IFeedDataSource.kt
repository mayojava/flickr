package com.mobile.app.flickr.data.source

import com.mobile.app.flickr.domain.entities.Feed
import com.mobile.app.flickr.domain.response.Response
import io.reactivex.Single

interface IFeedDataSource {
    fun getFeeds(): Single<Response<List<Feed>>>
}