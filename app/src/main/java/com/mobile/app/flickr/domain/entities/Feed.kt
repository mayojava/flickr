package com.mobile.app.flickr.domain.entities

data class Feed(
        val title: String,
        val link: String,
        val media: String,
        val description: String,
        val author: String,
        val published: String
)