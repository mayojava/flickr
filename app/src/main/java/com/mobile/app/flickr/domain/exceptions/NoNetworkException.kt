package com.mobile.app.flickr.domain.exceptions

class NoNetworkException: Exception {
    constructor():  super("No internet connection")

    constructor(cause: Throwable): super(cause)
}