package com.mobile.app.flickr.domain.interactor

import com.mobile.app.flickr.domain.entities.Feed
import com.mobile.app.flickr.domain.repository.IFeedRepository
import com.mobile.app.flickr.domain.repository.ISchedulersFactory
import com.mobile.app.flickr.domain.response.Response
import io.reactivex.FlowableSubscriber
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

class GetFeedsInteractor @Inject constructor(
        private val feedsRepository: IFeedRepository,
        private val schedulersFactory: ISchedulersFactory) {

    private val disposable = CompositeDisposable()

    fun execute(subscriber: DisposableObserver<Response<List<Feed>>>) {
        disposable.add(feedsRepository.getPublicFeeds()
                .subscribeOn(schedulersFactory.io())
                .observeOn(schedulersFactory.main())
                .subscribeWith(subscriber))
    }

    fun dispose() {
        if (!disposable.isDisposed) {
            disposable.dispose()
        }
    }
}