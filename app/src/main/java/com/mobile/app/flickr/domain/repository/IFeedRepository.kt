package com.mobile.app.flickr.domain.repository

import com.mobile.app.flickr.domain.entities.Feed
import com.mobile.app.flickr.domain.response.Response
import io.reactivex.Flowable
import io.reactivex.Observable

interface IFeedRepository {
    fun getPublicFeeds(): Observable<Response<List<Feed>>>
}