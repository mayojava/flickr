package com.mobile.app.flickr.domain.repository

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

interface ISchedulersFactory {
    fun main(): Scheduler

    fun io(): Scheduler

    fun computation(): Scheduler

    fun trampoline(): Scheduler
}