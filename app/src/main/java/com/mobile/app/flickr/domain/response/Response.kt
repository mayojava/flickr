package com.mobile.app.flickr.domain.response

sealed class Response<out T> {
    data class Success<out T>(val data: T): Response<T>()
    data class Error<out T>(val throwable: Throwable): Response<T>()
    class NoInternetError<out T>: Response<T>()
}