package com.mobile.app.flickr.injection

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import com.mobile.app.flickr.data.source.FeedLocalDataSource
import com.mobile.app.flickr.data.source.FeedRemoteDataSource
import com.mobile.app.flickr.data.source.FeedRepositoryImpl
import com.mobile.app.flickr.data.source.IFeedDataSource
import com.mobile.app.flickr.domain.repository.IFeedRepository
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class AppModule {
    @Provides
    @Singleton
    fun providesContext(application: Application): Context = application

    @Provides
    @Singleton
    fun providesConnectivityManager(context: Context): ConnectivityManager {
        return context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }

    @Provides
    @Singleton
    fun providesFeedsRepository(feedRepositoryImpl: FeedRepositoryImpl): IFeedRepository = feedRepositoryImpl

    @Provides
    @Singleton
    @Named("REMOTE")
    fun providesRemoteFeedDataSource(remoteDataSource: FeedRemoteDataSource): IFeedDataSource = remoteDataSource

    @Provides
    @Singleton
    @Named("LOCAL")
    fun providesLocalFeedDataSource(localDataSource: FeedLocalDataSource): IFeedDataSource = localDataSource
}