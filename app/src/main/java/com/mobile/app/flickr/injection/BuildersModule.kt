package com.mobile.app.flickr.injection

import com.mobile.app.flickr.presentation.feeds.FeedsActivity
import com.mobile.app.flickr.presentation.feeds.FeedsModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BuildersModule {
    @ContributesAndroidInjector(modules = arrayOf(FeedsModule::class))
    @PerActivity
    abstract fun bindFeedsActivity(): FeedsActivity
}