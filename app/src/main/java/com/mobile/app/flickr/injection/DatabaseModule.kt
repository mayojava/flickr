package com.mobile.app.flickr.injection

import android.arch.persistence.room.Room
import android.content.Context
import com.mobile.app.flickr.data.cache.CacheImpl
import com.mobile.app.flickr.data.cache.ICache
import com.mobile.app.flickr.data.db.FeedDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {
    @Provides
    @Singleton
    fun providesFeedsDatabase(context: Context): FeedDatabase {
        return Room.databaseBuilder(context, FeedDatabase::class.java, "flick_feeds").build()
    }

    @Provides
    @Singleton
    fun providesCache(cacheImpl: CacheImpl): ICache = cacheImpl
}