package com.mobile.app.flickr.injection

import android.content.Context
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.mobile.app.flickr.BuildConfig
import com.mobile.app.flickr.SchedulersFactory
import com.mobile.app.flickr.data.FlickrService
import com.mobile.app.flickr.domain.exceptions.NoNetworkException
import com.mobile.app.flickr.domain.repository.ISchedulersFactory
import com.mobile.app.flickr.presentation.utils.NetworkMonitor
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetworkModule {
    @Provides
    @Singleton
    fun providesSchedulersFactory(): ISchedulersFactory = SchedulersFactory()

    @Provides
    @Singleton
    fun providesFlickrService(retrofit: Retrofit): FlickrService  = retrofit.create(FlickrService::class.java)

    @Provides @Singleton fun providesRetrofit(
            okHttpClient: OkHttpClient,
            gson: Gson,
            @Named("base_url") baseUrl: String): Retrofit {

        return Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
    }

    @Provides @Singleton fun providesOkHttpClient(
            okHttpLoggingInterceptor: HttpLoggingInterceptor,
            @Named("InternetConnectionInterceptor") connectionInterceptor: Interceptor): OkHttpClient {

        return OkHttpClient.Builder()
                .addInterceptor(okHttpLoggingInterceptor)
                .addInterceptor(connectionInterceptor)
                .connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .build()
    }

    @Provides @Singleton fun providesGson(): Gson = GsonBuilder()
            .setLenient()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()

    @Provides @Singleton fun providesLogginInterceptor(@Named("isDebug") isDebug: Boolean): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = if (isDebug)
            HttpLoggingInterceptor.Level.BODY
        else
            HttpLoggingInterceptor.Level.NONE

        return logging
    }

    @Provides @Singleton @Named("InternetConnectionInterceptor")
    fun providesInternetConnectionInterceptor(networkMonitor: NetworkMonitor): Interceptor {
        return Interceptor { chain ->
            if (networkMonitor.isConnected()) {
                chain.proceed(chain.request())
            } else {
                throw NoNetworkException()
            }
        }
    }

    @Provides @Singleton fun providesCache(file: File) = Cache(file, 1024*1024*10)

    @Provides @Singleton fun providesCacheDir(context: Context): File = context.filesDir

    @Provides @Singleton @Named("isDebug") fun providesIsDebug(): Boolean = BuildConfig.DEBUG

    @Provides
    @Singleton
    @Named("base_url")
    fun providesBaseUrl(): String  = "https://api.flickr.com"
}