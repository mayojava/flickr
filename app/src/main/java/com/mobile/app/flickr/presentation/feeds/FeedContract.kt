package com.mobile.app.flickr.presentation.feeds

import android.support.annotation.StringRes
import com.mobile.app.flickr.domain.entities.Feed

interface FeedContract {
    interface View {
        fun showErrorMessage(@StringRes messageResource: Int)
        fun showErrorMessage(message: String)
        fun showProgress()
        fun hideProgress()
        fun displayFeeds(list: List<FeedViewModel>)
    }

    interface Presenter {
        fun detachView()
        fun fetchFeeds()
    }
}