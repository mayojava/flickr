package com.mobile.app.flickr.presentation.feeds

import android.os.Parcel
import android.os.Parcelable

data class FeedViewModel (
        val title: String,
        val link: String,
        val media: String,
        val description: String,
        val author: String,
        val published: String
): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString())


    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(link)
        parcel.writeString(media)
        parcel.writeString(description)
        parcel.writeString(author)
        parcel.writeString(published)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FeedViewModel> {
        override fun createFromParcel(parcel: Parcel): FeedViewModel {
            return FeedViewModel(parcel)
        }

        override fun newArray(size: Int): Array<FeedViewModel?> {
            return arrayOfNulls(size)
        }
    }
}