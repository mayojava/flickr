package com.mobile.app.flickr.presentation.feeds

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.PersistableBundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import com.mobile.app.flickr.R
import com.mobile.app.flickr.domain.entities.Feed
import com.mobile.app.flickr.presentation.utils.hide
import com.mobile.app.flickr.presentation.utils.show
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.reflect.Field
import javax.inject.Inject

class FeedsActivity: AppCompatActivity(), FeedContract.View, FeedsRecyclerAdapter.OnItemClickListener {

    @Inject lateinit var presenter: FeedContract.Presenter
    @Inject lateinit var feedsAdapter: FeedsRecyclerAdapter

    private val KEY_FEEDS = "FEEDS"

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupRecyclerView()
        setupToolbar()

        if (savedInstanceState != null) {
            val feeds = savedInstanceState.getParcelableArrayList<FeedViewModel>(KEY_FEEDS)
            feedsAdapter.setItems(feeds)
        } else {
            loadFeeds()
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        actionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun showErrorMessage(messageResource: Int) {
        Snackbar.make(root_view, messageResource, Snackbar.LENGTH_LONG).show()
    }

    override fun showErrorMessage(message: String) {
        Snackbar.make(root_view, message, Snackbar.LENGTH_LONG).show()
    }

    override fun showProgress() {
        loading_progress.show()
    }

    override fun hideProgress() {
        loading_progress.hide()
    }

    override fun displayFeeds(list: List<FeedViewModel>) {
        feedsAdapter.setItems(list)
    }

    override fun onItemClick(feed: FeedViewModel) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(feed.link)
        startActivity(intent)

    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelableArrayList(KEY_FEEDS, feedsAdapter.getItems())
        super.onSaveInstanceState(outState)
    }

    private fun loadFeeds() {
        presenter.fetchFeeds()
    }

    private fun setupRecyclerView() {
        with(recyclerview_feeds) {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = feedsAdapter
            addItemDecoration(DividerItemDecoration(context, (layoutManager as LinearLayoutManager).orientation))
        }
    }
}