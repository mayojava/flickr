package com.mobile.app.flickr.presentation.feeds

import com.mobile.app.flickr.injection.PerActivity
import com.mobile.app.flickr.presentation.utils.FeedsViewModelMapper
import dagger.Module
import dagger.Provides

@Module
class FeedsModule {

    @Provides
    @PerActivity
    fun providesFeedsActivity(feedsActivity: FeedsActivity): FeedContract.View = feedsActivity

    @Provides
    @PerActivity
    fun providesFeedsPresenter(presenter: FeedsPresenter): FeedContract.Presenter = presenter

    @Provides
    @PerActivity
    fun providesFeedsRecyclerAdapter(feedsActivity: FeedsActivity): FeedsRecyclerAdapter
            = FeedsRecyclerAdapter(feedsActivity)

    @Provides
    @PerActivity
    fun providesFeedsViewModelMapper(): FeedsViewModelMapper = FeedsViewModelMapper()
}