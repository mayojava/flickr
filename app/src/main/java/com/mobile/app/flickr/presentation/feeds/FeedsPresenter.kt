package com.mobile.app.flickr.presentation.feeds

import com.mobile.app.flickr.R
import com.mobile.app.flickr.data.cache.ICache
import com.mobile.app.flickr.domain.entities.Feed
import com.mobile.app.flickr.domain.interactor.GetFeedsInteractor
import com.mobile.app.flickr.domain.repository.ISchedulersFactory
import com.mobile.app.flickr.domain.response.Response
import com.mobile.app.flickr.presentation.utils.FeedsViewModelMapper
import io.reactivex.FlowableSubscriber
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import org.reactivestreams.Subscription
import java.lang.ref.WeakReference
import javax.inject.Inject

class FeedsPresenter @Inject constructor(
        feedsView: FeedContract.View,
        private val schedulersFactory: ISchedulersFactory,
        private val getFeedsInteractor: GetFeedsInteractor,
        private val viewModelMapper: FeedsViewModelMapper,
        private val cache: ICache): FeedContract.Presenter {

    var viewRef: WeakReference<FeedContract.View> = WeakReference(feedsView)

    override fun detachView() {
        viewRef.clear()
        getFeedsInteractor.dispose()
    }

    override fun fetchFeeds() {
        cache.isDbEmpty()
                .subscribeOn(schedulersFactory.io())
                .observeOn(schedulersFactory.main())
                .doOnSuccess { empty -> kotlin.run {
                    if (empty) {
                        viewRef.get()?.showProgress()
                    }

                    getFeedsInteractor.execute(FeedsObserver())
                } }
                .subscribe()
    }

    inner class FeedsObserver: DisposableObserver<Response<List<Feed>>>() {
        override fun onNext(t: Response<List<Feed>>) {
            return when(t) {
                is Response.Success -> viewRef.get()?.displayFeeds(viewModelMapper.mapToViewModel(t.data))!!
                is Response.NoInternetError -> viewRef.get()?.showErrorMessage(R.string.no_internet_connection)!!
                is Response.Error -> viewRef.get()?.showErrorMessage(t.throwable.toString())!!
            }
        }

        override fun onError(t: Throwable) {
            viewRef.get()?.hideProgress()
            viewRef.get()?.showErrorMessage(t.message?: "Unknown Error occurred")
        }

        override fun onComplete() {
            viewRef.get()?.hideProgress()
        }
    }
}