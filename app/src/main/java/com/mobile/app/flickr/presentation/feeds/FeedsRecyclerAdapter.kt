package com.mobile.app.flickr.presentation.feeds

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mobile.app.flickr.R
import com.mobile.app.flickr.presentation.utils.loadFromUrl
import kotlinx.android.synthetic.main.feeds_row_item.view.*

class FeedsRecyclerAdapter(private val listener: OnItemClickListener): RecyclerView.Adapter<FeedsRecyclerAdapter.ViewHolder>() {
    private var items = ArrayList<FeedViewModel>()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position], listener)
    }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.feeds_row_item, parent, false)
        return ViewHolder(view)
    }

    fun setItems(list: List<FeedViewModel>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }

    fun getItems(): ArrayList<FeedViewModel> = items

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bindItem(feed: FeedViewModel, listener: OnItemClickListener) {
            itemView.setOnClickListener { listener.onItemClick(feed) }
            itemView.image_view_feed_media.loadFromUrl(feed.media)
            itemView.text_view_feed_title.text = feed.title
            itemView.text_view_feed_author.text = String.format(itemView.context.getString(R.string.uploaded_by), feed.author)
            itemView.text_view_feed_published_date.text = String.format(itemView.context.getString(R.string.upload_published), feed.published)
        }
    }

    interface OnItemClickListener {
        fun onItemClick(feed: FeedViewModel)
    }
}