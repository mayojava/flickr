package com.mobile.app.flickr.presentation.utils

import com.mobile.app.flickr.domain.entities.Feed
import com.mobile.app.flickr.presentation.feeds.FeedViewModel

class FeedsViewModelMapper {

    fun mapToViewModel(domainList: List<Feed>): ArrayList<FeedViewModel> {
        val viewModels = ArrayList<FeedViewModel>()

        domainList.forEach { viewModels.add(
                FeedViewModel(
                        it.title,
                        it.link,
                        it.media,
                        it.description,
                        it.author,
                        it.published
                )
        ) }
        return viewModels
    }
}