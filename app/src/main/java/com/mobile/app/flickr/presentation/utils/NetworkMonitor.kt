package com.mobile.app.flickr.presentation.utils

import android.net.ConnectivityManager
import javax.inject.Inject

class NetworkMonitor @Inject constructor(
        private val connectivityManager: ConnectivityManager) {

    fun isConnected(): Boolean {
        val activeNetwork = connectivityManager.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }
}