package com.mobile.app.flickr

import com.mobile.app.flickr.domain.repository.ISchedulersFactory
import io.reactivex.Scheduler
import io.reactivex.schedulers.TestScheduler
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement

class RxJavaSchedulerRule: TestRule {
    private val ioScheduler = TestScheduler()
    private val mainScheduler = TestScheduler()
    private val computationScheduler = TestScheduler()
    private val trampolineScheduler = TestScheduler()
    private val iSchedulerFactory = createMockSchedulerFactory()

    override fun apply(base: Statement, description: Description?): Statement = base

    fun triggerMainScheduler() {
        mainScheduler.triggerActions()
    }

    fun triggerIOScheduler() {
        ioScheduler.triggerActions()
    }

    fun triggerComputationScheduler() {
        computationScheduler.triggerActions()
    }

    fun getSchedulersFactory(): ISchedulersFactory {
        return iSchedulerFactory
    }

    private fun createMockSchedulerFactory(): ISchedulersFactory {
        return object: ISchedulersFactory {
            override fun main(): Scheduler  = mainScheduler

            override fun io(): Scheduler = ioScheduler

            override fun computation(): Scheduler = computationScheduler

            override fun trampoline(): Scheduler = trampolineScheduler
        }
    }
}