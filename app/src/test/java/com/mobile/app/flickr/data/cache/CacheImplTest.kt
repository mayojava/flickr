package com.mobile.app.flickr.data.cache

import com.mobile.app.flickr.data.Mapper
import com.mobile.app.flickr.data.db.FeedDatabase
import com.mobile.app.flickr.data.db.FeedEntity
import com.mobile.app.flickr.data.db.FeedsDao
import com.mobile.app.flickr.domain.entities.Feed
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CacheImplTest {
    @Mock lateinit var feedDatabase: FeedDatabase
    @Mock lateinit var mapper: Mapper
    @Mock lateinit var feedsDao: FeedsDao

    private lateinit var cache: CacheImpl

    @Before
    fun setup() {
        cache = CacheImpl(feedDatabase, mapper)
        `when`(feedDatabase.feedsDao()).thenReturn(feedsDao)
    }

    @Test
    fun deleteFeedsShouldInteractWithDao() {
        //Arrange

        //Act
        cache.deleteFeeds()

        //Assert
        verify(feedDatabase).feedsDao()
        verify(feedsDao).deleteAllFeeds()
    }

    @Test
    fun testIsDbEmpty() {
        //Arrange
        `when`(feedsDao.recordsCount()).thenReturn(5)

        //Act
        val observer = cache.isDbEmpty().test()

        //Assert
        observer.assertNoErrors()
        observer.assertComplete()
        observer.assertValueCount(1)
        val response = observer.values()[0]
        assertFalse(response)
    }

    @Test
    fun insertFields_shouldCallMapperAndInsertIntoDao() {
        //Arrange
        val list = feedsList()

        //Act
        cache.insertFeeds(list)

        //Assert
        verify(mapper, times(2)).domainEntityToDbEntity(com.mobile.app.flickr.presentation.feeds.any())
        verify(feedsDao, times(2)).insertFeed(com.mobile.app.flickr.presentation.feeds.any())
    }

    @Test
    fun getFeeds_shouldGetFromFeedsDao() {
        //Arrange
        val entities = feedEntityList()
        `when`(feedsDao.getAllFeeds()).thenReturn(entities)

        //Act
        val observer = cache.getFeeds().test()

        //Assert
        observer.assertNoErrors()
        observer.assertComplete()
        observer.assertValueCount(1)

        verify(feedDatabase).feedsDao()
        verify(feedsDao).getAllFeeds()
        verify(mapper, times(2)).dbEntityToDomain(com.mobile.app.flickr.presentation.feeds.any())
    }

    private fun feedEntityList(): List<FeedEntity> {
        val entity1 = FeedEntity(1, "title", "link", "media",
                "description", "author", "published")

        val entity2 = FeedEntity(1, "title", "link", "media",
                "description", "author", "published")

        return listOf(entity1, entity2)
    }

    private fun feedsList(): List<Feed> {
        val feed1 = Feed("title", "link", "media",
                "description", "author", "published")
        val feed2 = Feed("title 2", "link 2", "media 2",
                "description 2", "author 2", "published 2")

        return listOf(feed1, feed2)
    }
}