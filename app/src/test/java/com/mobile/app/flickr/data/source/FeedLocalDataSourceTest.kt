package com.mobile.app.flickr.data.source

import com.mobile.app.flickr.data.cache.ICache
import com.mobile.app.flickr.domain.entities.Feed
import com.mobile.app.flickr.domain.response.Response
import com.mobile.app.flickr.presentation.feeds.mock
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*

import org.mockito.junit.MockitoJUnitRunner
import kotlin.test.assertTrue

@RunWith(MockitoJUnitRunner::class)
class FeedLocalDataSourceTest {
    @Mock lateinit var cache: ICache

    private lateinit var feedLocalDataSource: FeedLocalDataSource

    @Before
    fun setup() {
        feedLocalDataSource = FeedLocalDataSource(cache)
    }

    @Test
    fun testGetFeeds_fetchesFromCache() {
        //Arrange
        val feeds = getFeedsList()
        `when`(cache.getFeeds()).thenReturn(Single.just(feeds))

        //Act
        val observer = feedLocalDataSource.getFeeds().test()

        //Assert
        observer.assertNoErrors()
        observer.assertComplete()
        observer.assertValueCount(1)

        val response = observer.values()[0]
        assertTrue { response is Response.Success }
    }

    fun getFeedsList(): List<Feed> {
        val feed1 = mock<Feed>()
        val feed2 = mock<Feed>()

        return listOf<Feed>(feed1, feed2)
    }
}