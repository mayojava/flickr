package com.mobile.app.flickr.data.source

import com.mobile.app.flickr.data.FlickrService
import com.mobile.app.flickr.data.cache.ICache
import com.mobile.app.flickr.data.entities.ApiResponse
import com.mobile.app.flickr.data.entities.FeedItem
import com.mobile.app.flickr.data.entities.Media
import com.mobile.app.flickr.domain.exceptions.NoNetworkException
import com.mobile.app.flickr.domain.response.Response
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.*

import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class FeedRemoteDataSourceTest {
    @Mock lateinit var flickrService: FlickrService
    @Mock lateinit var cache: ICache

    private lateinit var feedRemoteDataSource: FeedRemoteDataSource

    @Before
    fun setup() {
        feedRemoteDataSource = FeedRemoteDataSource(flickrService, cache)
    }

    @Test
    fun testGetFeeds_onSuccess_SavesToCache() {
        //Arrange
        val api = getApiResponse()
        `when`(flickrService.fetchPublicFeed()).thenReturn(Single.just(api))

        //Act
        val observer = feedRemoteDataSource.getFeeds().test()

        //Assert
        observer.assertNoErrors()
        observer.assertComplete()
        observer.assertValueCount(1)
        verify(cache).deleteFeeds()
        verify(cache).insertFeeds(ArgumentMatchers.anyList())
    }

    @Test
    fun testErrorComesBackInErrorBundle() {
        //Arrange
        `when`(flickrService.fetchPublicFeed()).thenReturn(Single.error(NoNetworkException()))

        //Act
        val observer = feedRemoteDataSource.getFeeds().test()

        //Assert
        observer.assertNoErrors()
        observer.assertComplete()
        observer.assertValueCount(1)

        val response = observer.values()[0]
        kotlin.test.assertTrue { response is Response.NoInternetError }
    }

    fun getApiResponse(): ApiResponse {
        val feedItem1 = FeedItem("title 1", "link 1", Media("media 1"),
                "2017-09-10", "hello world",
                "published", "James", "1")
        val feedItem2 = FeedItem("title 2", "link 2", Media("media 2"),
                "2017-09-10", "hello world",
                "published", "Baron", "2")

        val response = ApiResponse("title", "link",
                "description", "modified", "generator",
                listOf(feedItem1, feedItem2))

        return response
    }
}