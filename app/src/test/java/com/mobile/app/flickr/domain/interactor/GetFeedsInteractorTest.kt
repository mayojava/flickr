package com.mobile.app.flickr.domain.interactor

import com.mobile.app.flickr.RxJavaSchedulerRule
import com.mobile.app.flickr.domain.entities.Feed
import com.mobile.app.flickr.domain.repository.IFeedRepository
import com.mobile.app.flickr.domain.response.Response
import com.mobile.app.flickr.presentation.feeds.mock
import io.reactivex.Observable
import io.reactivex.observers.DisposableObserver
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetFeedsInteractorTest {
    @Mock lateinit var feedsRepository: IFeedRepository

    @Rule @JvmField var rxJavaSchedulerRule = RxJavaSchedulerRule()

    private lateinit var getFeedsInteractor: GetFeedsInteractor

    @Before
    fun setup() {
        getFeedsInteractor = GetFeedsInteractor(feedsRepository, rxJavaSchedulerRule.getSchedulersFactory())
    }

    @Test
    fun testExecute_shouldCallRepository() {
        //Arrange
        `when`(feedsRepository.getPublicFeeds()).thenReturn(Observable.just(getFeedsResponse()))
        val mock = mock<DisposableObserver<Response<List<Feed>>>>()

        //Act
        getFeedsInteractor.execute(mock)

        //Aseert
        verify(feedsRepository).getPublicFeeds()
    }

    @Test
    fun testExecute_shouldCallONNextOnSubscriber_whenItEmits() {
        //Arrange
        val response = getFeedsResponse()
        `when`(feedsRepository.getPublicFeeds()).thenReturn(Observable.just(response))
        val mockObserver = mock<DisposableObserver<Response<List<Feed>>>>()

        //Act
        getFeedsInteractor.execute(mockObserver)

        rxJavaSchedulerRule.triggerIOScheduler()
        rxJavaSchedulerRule.triggerMainScheduler()

        //Assert
        verify(feedsRepository).getPublicFeeds()
        verify(mockObserver).onNext(response)
        verify(mockObserver).onComplete()
        verifyNoMoreInteractions(feedsRepository)
    }

    fun getFeedsResponse(): Response<List<Feed>> {
        val feed1 = mock<Feed>()
        val feed2 = mock<Feed>()

        val list = listOf<Feed>(feed1, feed2)
        return Response.Success(list)
    }
}