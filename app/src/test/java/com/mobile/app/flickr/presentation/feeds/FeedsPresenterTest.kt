package com.mobile.app.flickr.presentation.feeds

import com.mobile.app.flickr.R
import com.mobile.app.flickr.RxJavaSchedulerRule
import com.mobile.app.flickr.data.cache.ICache
import com.mobile.app.flickr.domain.entities.Feed
import com.mobile.app.flickr.domain.interactor.GetFeedsInteractor
import com.mobile.app.flickr.domain.repository.IFeedRepository
import com.mobile.app.flickr.domain.response.Response
import com.mobile.app.flickr.presentation.utils.FeedsViewModelMapper
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class FeedsPresenterTest {
    @Mock lateinit var viewModelMapper: FeedsViewModelMapper
    @Mock lateinit var feedsRepository: IFeedRepository
    @Mock lateinit var cache: ICache
    @Mock lateinit var view: FeedContract.View

    lateinit var getFeedsInteractor: GetFeedsInteractor

    @Rule @JvmField val rxJavaSchedulerRule = RxJavaSchedulerRule()

    private lateinit var presenter: FeedsPresenter


    @Before
    fun setup() {
        getFeedsInteractor = GetFeedsInteractor(feedsRepository, rxJavaSchedulerRule.getSchedulersFactory())
        presenter = FeedsPresenter(view, rxJavaSchedulerRule.getSchedulersFactory(), getFeedsInteractor, viewModelMapper, cache)
    }

    @Test
    fun testFetchFeeds_shouldShowLoadingAndFetchResults_whenLocalCacheIsEmpty() {
        //Arrange
        val response = getFeedsResponse()
        `when`(cache.isDbEmpty()).thenReturn(Single.just(true))
        `when`(feedsRepository.getPublicFeeds()).thenReturn(Observable.just(response))

        //Act
        presenter.fetchFeeds()
        triggerCacheAndInteractorSchedulers()


        //Assert
        verify(cache).isDbEmpty()
        verify(view).showProgress()
        verify(view).hideProgress()
        verify(viewModelMapper).mapToViewModel(ArgumentMatchers.anyList())
    }

    private fun triggerCacheAndInteractorSchedulers() {
        rxJavaSchedulerRule.triggerIOScheduler()
        rxJavaSchedulerRule.triggerMainScheduler()

        rxJavaSchedulerRule.triggerIOScheduler()
        rxJavaSchedulerRule.triggerMainScheduler()
    }

    @Test
    fun doesNotShowProgress_whenCacheIsNotEmpty() {
        //Arrange
        val response = getFeedsResponse()
        `when`(cache.isDbEmpty()).thenReturn(Single.just(false))
        `when`(feedsRepository.getPublicFeeds()).thenReturn(Observable.just(response))

        //Act
        presenter.fetchFeeds()

        rxJavaSchedulerRule.triggerIOScheduler()
        rxJavaSchedulerRule.triggerMainScheduler()

        //Assert
        verify(cache).isDbEmpty()
        verify(view, never()).showProgress()
    }

    @Test
    fun shouldShowErrorMessage_whenResponseComesBackWithExpectedError() {
        //Arrange
        `when`(cache.isDbEmpty()).thenReturn(Single.just(false))
        `when`(feedsRepository.getPublicFeeds()).thenReturn(Observable.just(Response.NoInternetError()))

        //Act
        presenter.fetchFeeds()
        triggerCacheAndInteractorSchedulers()

        //Assert
        verify(cache).isDbEmpty()
        verify(view, never()).showProgress()
        verify(view).showErrorMessage(R.string.no_internet_connection)

    }

    fun getFeedsResponse(): Response<List<Feed>> {
        val feed1 = mock<Feed>()
        val feed2 = mock<Feed>()

        val list = listOf<Feed>(feed1, feed2)
        return Response.Success(list)
    }
}